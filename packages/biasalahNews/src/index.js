import Theme from "./components";

export default {
  name: "biasalahNews",
  roots: {
    theme: Theme,
  },
  state: {
    theme: {
      menu: [],
    },
  },
  actions: {
    theme: {},
  },
};
