import Script from "@frontity/components/script";
import { Global, css, connect, styled, Head } from "frontity";
import StyleCss from "./styles/style.css";
import Preloader from "./Preloader";
import Header from "./header/header";
import Footer from "./footer/footer";
import CustomJs from "./js/custom";
import Home from "./pages/home";
import PopUp from "./popup";
import CanvasMenu from "./canvas";

const Theme = ({ state }) => {
  // Get information about the current URL.
  const data = state.source.get("/");
  console.log(data);
  return (
    <>
      {/* Add some metatags to the <head> of the HTML. */}
      <Head>
        <title>Biaslah</title>
        <meta name="description" content="Meta deskripsi biasalah" />
        <html lang="en" />
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
        ></link>
        <link
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
          rel="stylesheet"
        ></link>
        <link
          href="https://cdn.jsdelivr.net/npm/simple-line-icons@2.4.1/css/simple-line-icons.css"
          rel="stylesheet"
        ></link>
        <link
          href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
          rel="stylesheet"
        ></link>
      </Head>
      <Global styles={css(StyleCss)} />

      {/* <Preloader /> */}

      <div class="site-wrapper">
        <div class="main-overlay"></div>
        <Header />
        <Home />
        <Footer />
      </div>
      <PopUp />
      <CanvasMenu />

      <Script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
      />
      <Script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
      />
      <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" />
      <Script
        src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"
        integrity="sha512-An4a3FEMyR5BbO9CRQQqgsBscxjM7uNNmccUSESNVtWn53EWx5B9oO7RVnPvPG6EcYcYPp0Gv3i/QQ4KUzB5WA=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
      />
      <Script
        src="https://cdnjs.cloudflare.com/ajax/libs/sticky-sidebar/3.3.1/sticky-sidebar.min.js"
        integrity="sha512-iVhJqV0j477IrAkkzsn/tVJWXYsEqAj4PSS7AG+z1F7eD6uLKQxYBg09x13viaJ1Z5yYhlpyx0zLAUUErdHM6A=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
      />
      <Script code={CustomJs} />
    </>
  );
};

export default connect(Theme);
