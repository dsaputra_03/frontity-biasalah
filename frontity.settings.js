const settings = {
  name: "frontity-biasalah",
  state: {
    frontity: {
      url: "https://test.frontity.org",
      title: "Test Frontity Blog",
      description: "WordPress installation for Frontity development",
    },
  },
  packages: [
    {
      name: "biasalahNews",
    },
    {
      name: "@frontity/wp-source",
      state: {
        source: {
          url: "https://biasalah.news/",
        },
      },
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
  ],
};

export default settings;
